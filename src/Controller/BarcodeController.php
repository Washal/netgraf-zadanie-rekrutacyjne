<?php

namespace App\Controller;

use App\Entity\Barcode;
use App\Form\BarcodeFormType;
use App\Repository\BarcodeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Picqer\Barcode\BarcodeGeneratorPNG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BarcodeController extends AbstractController
{
    #[Route('/', name: 'app_barcode')]
    public function index(Request $request, EntityManagerInterface $entityManager, BarcodeRepository $barcodeRepository): Response
    {
        $barcode = new Barcode();
        $form = $this->createForm(BarcodeFormType::class, $barcode);

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()){

            //check if we already have that barcode in database
            if(is_null($barcodeRepository->findOneBy(['content' => $form->get('content')->getData()]))) {

                //generate png image of barcode
                $barcodeGenerator = new BarcodeGeneratorPNG();
                file_put_contents('img/'.$barcode->getContent().'.png',
                    $barcodeGenerator->getBarcode(
                        $barcode->getContent(),
                        $barcodeGenerator::TYPE_CODE_128,
                        3,
                        50)
                );

                $barcode->setImageUrl(
                    self::webpImage('img/'.$barcode->getContent().'.png')
                );
                //save barcode info to database
                $entityManager->persist($barcode);
                $entityManager->flush();

                return $this->render('barcode/index.html.twig', [
                    'addBarcode' => $form->createView(),
                    'barcode' => $barcode,
                    'message' => 'Successfully generated your barcode!'
                ]);

            }
            else{
                //send existing barcode to user
                return $this->render('barcode/index.html.twig', [
                    'addBarcode' => $form->createView(),
                    'barcode' => $barcodeRepository->findOneBy(['content' => $form->get('content')->getData()]),
                    'message' => 'Successfully generated your barcode!'
                ]);

            }


        }

        return $this->render('barcode/index.html.twig', [
            'addBarcode' => $form->createView(),
        ]);
    }
    //convert png to webp and return path
    public static function webpImage($source, $quality = 100): string
    {
        $dir = pathinfo($source, PATHINFO_DIRNAME);
        $name = pathinfo($source, PATHINFO_FILENAME);
        $destination = $dir . DIRECTORY_SEPARATOR . $name . '.webp';

        $image = imagecreatefrompng($source);
        imagepalettetotruecolor($image);
        imagealphablending($image, true);
        imagesavealpha($image, true);
        imagewebp($image, $destination, $quality);
        unlink($source);

        return $destination;
    }


}
