# Barcode generator

Generates barcode Code-128 from text and saving it as WebP image

Using picqer/php-barcode-generator to generate png, then converting it to WebP format

# Demo
http://netgraf.webvision.art/
